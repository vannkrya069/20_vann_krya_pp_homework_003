let date = "";
currentDate = () => {
  date = new Date(Date.now());
  return (
    date
      .toLocaleDateString([], {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
      })
      .replaceAll(",", " ") +
    " " +
    date.toLocaleTimeString()
  );
};
setInterval(function () {
  document.getElementById("time").innerHTML = currentDate();
}, 1000);

startTime = () => {
  catchStart = date;
  document.getElementById("timeStart").innerHTML =
    catchStart.toLocaleTimeString([], {
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
};

stopTime = () => {
  catchStop = date;
  document.getElementById("timeStop").innerHTML = catchStop.toLocaleTimeString(
    [],
    {
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    }
  );
  //   console.log(catchStop - catchStart)
  minutesCalculate();
};

clearTime = () => {
  document.getElementById("timeStart").innerHTML = "0:00";
  document.getElementById("timeStop").innerHTML = "0:00";
  document.getElementById("minutesPlay").innerHTML = "0";
  document.getElementById("totalCashes").innerHTML = "0";
};

let catchStart = "";
let catchStop = "";
minutesCalculate = () => {
  let catchAll = catchStop - catchStart;
  let Seconds = catchAll / 1000;
  let Minutes = Seconds / 60;
  //   let Minutes = 60;
  let paid = "";
  document.getElementById("minutesPlay").innerHTML = parseInt(Minutes);
  if (parseInt(Minutes) == 0) {
    paid = 500;
    console.log(paid);
  } else if (parseInt(Minutes) % 60 == 0) {
    paid = (parseInt(Minutes) / 60) * 1500;
    console.log(paid);
  } else if (parseInt(Minutes) % 60 <= 15) {
    paid = (parseInt(Minutes) / 60) * 1500 + 500;
    console.log(paid);
  } else if (parseInt(Minutes) % 60 <= 30) {
    paid = (parseInt(Minutes) / 60) * 1500 + 1000;
    console.log(paid);
  } else {
    paid = (parseInt(Minutes) / 60) * 1500 + 1500;
    console.log(paid);
  }

  document.getElementById("totalCashes").innerHTML =
    parseFloat(paid).toFixed(0);
};

let start = false;
let stop = true;
let clear = false;
clickBtn = () => {
  if (start) {
    document.getElementById("myBtn").className =
      "text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 font-bold rounded-lg text-lg px-20 py-2 text-center mr-2 mb-2";
    document.getElementById("start").innerHTML = "Start";
    document.getElementById("iconStart").className =
      "fa-solid fa-play mt-1 mr-3";
    clearTime();
    start = false;
    stop = true;
    clear = false;
  } else if (stop) {
    document.getElementById("myBtn").className =
      "text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 font-bold rounded-lg text-lg px-20 py-2 text-center mr-2 mb-2";
    document.getElementById("start").innerHTML = "Stop";
    document.getElementById("iconStart").className =
      "fa-solid fa-circle-stop mt-1 mr-3";
    startTime();
    start = false;
    stop = false;
    clear = true;
  } else if (clear) {
    document.getElementById("myBtn").className =
      "text-white bg-gradient-to-r from-yellow-400 via-yellow-500 to-yellow-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-yellow-300 font-bold rounded-lg text-lg px-20 py-2 text-center mr-2 mb-2";
    document.getElementById("start").innerHTML = "Clear";
    document.getElementById("iconStart").className =
      "fa-solid fa-trash mt-1 mr-3";
    stopTime();
    start = true;
    stop = false;
    clear = false;
  }
};
